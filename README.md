<p align="center"><a href="https://gotm.io/nnbgames/the-pearl-of-time"><img src="thumbnailx2.png"></a></p>
<p align="center">A short adventure of a time travel pearl on a small island</p>

## 💡 About

This is made with [Godot 3](https://godotengine.org/download/3.x) and is a game jam entry for [Gotm Jam #25](https://gotm.io/jam/gotm-jam/25).

<br>
<br>
<p align="center"><a href="https://odysee.com/@nnb:3/the-pearl-of-time-walkthrough:8">Walkthrough</a></p>

## 🎮 Controls

- Left-right arrows (or <kbd>A</kbd> <kbd>D</kbd>, <kbd>J</kbd> <kbd>L</kbd>) to move.
- <kbd>Space</kbd> or <kbd>C</kbd> to jump.
- <kbd>R</kbd> to retry level.
- <kbd>F11</kbd> to toggle full-screen.

## 💌 Credits

- [God rays shaders](https://godotshaders.com/shader/god-rays) by [Peter Höglund](https://godotshaders.com/author/pend00).
- [Cozette font](https://github.com/slavfox/Cozette) by [Slavfox](https://github.com/slavfox).
- Music and Sound from [Mixkit](https://mixkit.co).
- [Background](https://unsplash.com/photos/IRpqqevrPvM) by [Tony Pham](https://unsplash.com/@tonyphamvn).
- Textures:
  - [Stone Brick Wall Golgotha](https://opengameart.org/node/7669) from Crack.com.
  - [Partially mossy concrete wall](https://opengameart.org/content/partially-mossy-concrete-wall-512px) by Tiziana.
  - [Plants textures Pack](https://opengameart.org/content/plants-textures-pack-01) by [Yughues](https://opengameart.org/users/yughues).
  - [ZRPG Beach](https://opengameart.org/content/zrpg-beach) by [Zachariah Husiar](https://opengameart.org/users/zabin).
  - [Vines](https://opengameart.org/content/vines-0) by [KnoblePersona](https://opengameart.org/users/knoblepersona).
  - [Statues & Fountains Collection](https://opengameart.org/content/statues-fountains-collection) by [Jordan Irwin](https://opengameart.org/users/antumdeluge).
  - Other from [Benkyou Studio's textures](http://www.benkyoustudio.com/Textures).
- Converted using [SLK_img2pixel](https://captain4lk.itch.io/slk-img2pixel).
