extends Node2D


var entered := false
var stage := 2

onready var SCREEN_CHECKPOINT : Vector2 = $"../Screen0/Area2D/Checkpoint".get_global_transform().origin


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("checkpoint", self, "_on_checkpoint")
	# warning-ignore:return_value_discarded
	Global.connect("time_travel_forward", self, "_on_time_travel_forward")
	# warning-ignore:return_value_discarded
	Global.connect("time_travel_backward", self, "_on_time_travel_backward")
	
	time_travel()


func time_travel() -> void:
	for i in range(0, 5):
		get_node(str(i)).position = Vector2.ZERO if i == stage else Vector2(0, -512)


func _on_checkpoint(position) -> void:
	if not entered and position == SCREEN_CHECKPOINT:
		entered = true

func _on_time_travel_forward() -> void:
	if entered and stage < 4:
		stage += 1
		time_travel()

func _on_time_travel_backward() -> void:
	if entered and stage > 0:
		stage -= 1
		time_travel()
