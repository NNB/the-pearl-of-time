extends Node2D


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("time_travel", self, "_on_time_travel")
	
	_on_time_travel(Global.month)


func _on_time_travel(month: int) -> void:
	var opening := month >= 4 and month <= 6
	$Close.toggle(!opening)
	$Open.visible = opening
