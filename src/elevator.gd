extends Node2D


var running_month := 0

onready var SCREEN_CHECKPOINT : Vector2 = $Screen1/Area2D/Checkpoint.get_global_transform().origin


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("checkpoint", self, "_on_checkpoint")
	# warning-ignore:return_value_discarded
	Global.connect("time_travel", self, "_on_time_travel")
	
	_on_time_travel(Global.month)


func _on_checkpoint(position) -> void:
	if running_month == 0 and position == SCREEN_CHECKPOINT:
		running_month = (Global.month + 6) % 12
		$Note.text = "Elevator only run in %s." % Global.MONTHS[running_month - 1]

func _on_time_travel(month: int) -> void:
	$Elevator/AnimationPlayer.play("running" if month == running_month else "RESET")
