extends Area2D


var ended := false


func _on_Ending_body_entered(_body) -> void:
	if not ended:
		ended = true
		$AnimationPlayer.play("ending")
