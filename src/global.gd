extends Node


const MONTHS = [
	"January", "February", "March",
	"April",   "May",      "June",
	"July",    "August",   "September",
	"October", "November", "December"
]

var checkpoint : Vector2
var month := 1

# warning-ignore:unused_signal
signal checkpoint(position)
signal retry()
signal time_travel(month)
signal time_travel_forward()
signal time_travel_backward()


func _ready() -> void:
	# warning-ignore:return_value_discarded
	connect("checkpoint", self, "_on_checkpoint")

func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("time_travel_forward"):
		month = month + 1 if month < 12 else 1
		emit_signal("time_travel", month)
		emit_signal("time_travel_forward")
	if Input.is_action_just_pressed("time_travel_backward"):
		month = month - 1 if month > 1 else 12
		emit_signal("time_travel", month)
		emit_signal("time_travel_backward")
	if Input.is_action_just_pressed("retry"):
		emit_signal("retry")
	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen

func _on_checkpoint(position: Vector2) -> void:
	if position != checkpoint:
		checkpoint = position
