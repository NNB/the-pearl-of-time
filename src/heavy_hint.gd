extends Label


var displayed := false

onready var SCREEN_CHECKPOINT : Vector2 = $"../Screen1/Area2D/Checkpoint".get_global_transform().origin


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("checkpoint", self, "_on_checkpoint")


func _on_checkpoint(position: Vector2) -> void:
	if displayed == false and position == SCREEN_CHECKPOINT:
		$AnimationPlayer.play("display")
		displayed = true

func _on_Area2D_body_entered(_body) -> void:
	$AnimationPlayer.playback_speed = -1
