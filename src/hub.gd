extends CanvasLayer


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("time_travel", self, "_on_time_travel")


func _on_time_travel(month: int) -> void:
	$Control/Label.text = "%02d (%s)" % [month, Global.MONTHS[month - 1]]
	$Control/Label/AnimationPlayer.stop()
	$Control/Label/AnimationPlayer.play("display")
