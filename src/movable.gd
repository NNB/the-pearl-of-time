extends RigidBody2D


var original_transform : Transform2D
onready var original_mass := mass
var retry := false


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("retry", self, "_on_retry")
	# warning-ignore:return_value_discarded
	Global.connect("time_travel_backward", self, "_on_retry")
	# warning-ignore:return_value_discarded
	Global.connect("time_travel_forward", self, "_on_time_travel_forward")

func _physics_process(delta) -> void:
	mass = min(mass + delta * 16, original_mass)

func _integrate_forces(state) -> void:
	if not original_transform:
		original_transform = state.transform
	
	if not retry or not original_transform:
		return
	
	state.transform = original_transform
	retry = false


func _on_time_travel_forward() -> void:
	mass = 1

func _on_retry() -> void:
	retry = true
	set_deferred("mode", RigidBody2D.MODE_RIGID)
