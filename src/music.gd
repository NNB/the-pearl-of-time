extends AudioStreamPlayer


var play := false

onready var START_POINT : Vector2 = $"../0/Screen0/Area2D/Checkpoint".get_global_transform().origin


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("checkpoint", self, "_on_checkpoint")

func _process(delta) -> void:
	volume_db = lerp(volume_db, 0 if play else -80, delta)


func _on_checkpoint(position: Vector2) -> void:
	play = position != START_POINT
