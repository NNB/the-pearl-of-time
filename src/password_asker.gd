extends Node2D


var password := []


func _on_Button_body_entered(_body) -> void:
	if $AnimationPlayer.is_playing():
		return
	
	password.push_back("%02d" % Global.month)
	$Face/Mouth/Password.text = " ".join(password)
	
	if len(password) < 3:
		return
	
	if $Face/Mouth/Password.text == "06 03 09":
		$AnimationPlayer.play("down")
	else:
		$AnimationPlayer.play("error")
		password = []
		$Face/Mouth/Password.text = ""
