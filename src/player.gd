extends RigidBody2D


const SPEED = 3 * pow(8, 3)
const JUMP_HEIGHT = 24 * pow(8, 3)

var directions = [Vector2.ZERO]
var floors := 0
var screens = []


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("retry", self, "_on_retry")

func _process(_delta: float) -> void:
	update_camera()

func _physics_process(delta: float) -> void:
	$FloorDetector.rotation_degrees = -rotation_degrees
	moving(delta)
	jumping(delta)


func is_on_floor() -> bool:
	return floors >= 1

func moving(delta: float) -> void:
	if Input.is_action_just_pressed("ui_left"):
		directions.push_front(Vector2.LEFT)
	elif Input.is_action_just_pressed("ui_right"):
		directions.push_front(Vector2.RIGHT)
	
	if Input.is_action_just_released("ui_left"):
		directions.erase(Vector2.LEFT)
	elif Input.is_action_just_released("ui_right"):
		directions.erase(Vector2.RIGHT)
	
	if Input.is_action_pressed("ui_left") and directions[0] == Vector2.LEFT:
		apply_central_impulse(Vector2(-SPEED, 0) * delta)
		return
	
	if Input.is_action_pressed("ui_right") and directions[0] == Vector2.RIGHT:
		apply_central_impulse(Vector2(SPEED, 0) * delta )
		return
	
	directions = [Vector2.ZERO]

func jumping(delta: float) -> void:
	if is_on_floor():
		$CoyoteTimer.start()
	elif $CoyoteTimer.is_stopped():
		if Input.is_action_pressed("jump"):
			$JumpBufferTimer.start()
		
		return
	
	if Input.is_action_just_pressed("jump") or not $JumpBufferTimer.is_stopped():
		set_axis_velocity(Vector2(0, -JUMP_HEIGHT) * delta)
		$JumpBufferTimer.stop()
		$CoyoteTimer.stop()

func update_camera() -> void:
	if not screens:
		return
	
	var collision_shape : CollisionShape2D = screens[0].get_node("CollisionShape2D")
	var collision_origin : Vector2 = collision_shape.get_global_transform().origin
	var collision_extents : Vector2 = collision_shape.shape.extents
	
	$Camera2D.limit_left   = collision_origin.x - collision_extents.x
	$Camera2D.limit_right  = collision_origin.x + collision_extents.x
	$Camera2D.limit_top    = collision_origin.y - collision_extents.y
	$Camera2D.limit_bottom = collision_origin.y + collision_extents.y
	
	Global.emit_signal(
		"checkpoint",
		screens[0].get_node("Checkpoint").get_global_transform().origin
	)


func _on_FloorDetector_body_entered(_body) -> void:
	floors += 1

func _on_FloorDetector_body_exited(_body) -> void:
	floors -= 1

func _on_ScreenDetector_area_entered(area) -> void:
	screens.push_front(area)

func _on_ScreenDetector_area_exited(area) -> void:
	screens.erase(area)

func _on_DeadlyDetector_body_entered(_body) -> void:
	Global.emit_signal("retry")

func _on_retry() -> void:
	set_deferred("mode", RigidBody2D.MODE_STATIC)
	set_deferred("position", Global.checkpoint)
	set_deferred("mode", RigidBody2D.MODE_RIGID)
