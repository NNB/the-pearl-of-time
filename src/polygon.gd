extends Polygon2D


export var outline_color : Color
export var deadly := false

onready var static_body := StaticBody2D.new()
onready var collision_polygon := CollisionPolygon2D.new()
onready var outline := Line2D.new()


func _ready() -> void:
	static_body.set_collision_layer_bit(0, false)
	static_body.set_collision_mask_bit(0, false)
	toggle(true)
	add_child(static_body)
	
	collision_polygon.polygon = polygon
	static_body.add_child(collision_polygon)
	
	var points := polygon
	points.push_back(polygon[0])
	outline.points = points
	outline.width = 2
	outline.default_color = outline_color
	add_child(outline)


func toggle(enable: bool) -> void:
	visible = enable
	static_body.set_collision_layer_bit(2 if deadly else 1, enable)
