extends Node2D


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("time_travel", self, "_on_time_travel")
	
	_on_time_travel(Global.month)


func _on_time_travel(month: int) -> void:
	for i in range(1, 13):
		var polygon = get_node(str(i))
		if polygon:
			polygon.toggle(i == month)
