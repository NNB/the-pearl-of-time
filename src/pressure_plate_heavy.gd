extends Area2D


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("retry", self, "_on_retry")
	# warning-ignore:return_value_discarded
	Global.connect("time_travel_backward", self, "_on_retry")


func toggle(opening) -> void:
	$"../Door/AnimationPlayer".play("open" if opening else "RESET")


func _on_PressurePlateHeavy_body_entered(_body) -> void:
	toggle(true)

func _on_retry() -> void:
	toggle(false)
