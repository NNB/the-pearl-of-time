extends Node2D


var type := 1


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Global.connect("time_travel", self, "_on_time_travel")
	
	_on_time_travel()


func _on_time_travel(_month = null) -> void:
	type = 1 if type == 0 else 0
	for i in [0, 1]:
		get_node(str(i)).position = Vector2.ZERO if i == type else Vector2(0, -512)
